//Check that in subclasses, it can reference and set super class fields where approperaiate...


class Main
{
    public static void main (String[] args)
    {
    	A a;
    	a = new B();
    	//Call once to get parameter
    	a = a.ret();
    	//Call again to make sure call site isn't null
    	a = a.ret();
    }
}

class A
{
	A a;
	public int init()
	{
		a = new A();
		return 0;
	}
	public A ret()
	{
		int i;
		a = new A();
		i = this.init();
		return a;
	}
}
class B extends A
{
	public int init()
	{
	    a = new A();
		return 1;
	}
	public A ret()
	{
		int i;
		a = new A();
		i = this.init();
		return a;
	}
}